<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="tis-620">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<meta http-equiv="cache-control" content="max-age=0" />
		<meta http-equiv="cache-control" content="no-cache" />
		<meta http-equiv="expires" content="0" />
		<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
		<meta http-equiv="pragma" content="no-cache" />
		
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>.:Add word to Dictionary:.</title>

		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class='container'>
			<form action="save_json.php" method="post">
			<div class='row'>
				<div class='col-md-12'>
					<h3><center>Add Vocab</center></h3>
					<h4><center>Please fill in the form completely. If not available use (-).</center></h4>
				</div>
			</div>
			<div class='row'>
				<div class='col-md-12'>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Verb</h3>
						</div>
						<div class="panel-body">
							<div class='col-sm-4'>
								<button type="button" class="btn btn-default">+</button><br>
								Translate1: <input type="text" name="vtranslate1" id="translate" class="form-control"/>
							</div>
							
							<div class='col-sm-4'>
							<button type="button" class="btn btn-default">+</button><br>
								Synonyms1: <input type="text" name="vsynonyms1" id="synonyms" class="form-control"/>
							</div>
							
							<div class='col-sm-4'>
							<button type="button" class="btn btn-default">+</button><br>
								Antonyms1: <input type="text" name="vantonyms1" id="antonyms" class="form-control"/>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class='row'>
				<div class='col-md-12'>
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3 class="panel-title">Noun</h3>
						</div>
						<div class="panel-body">
							<div class='col-sm-4'>
								<button type="button" class="btn btn-success">+</button><br>
								Translate1: <input type="text" name="vtranslate1" id="translate" class="form-control"/>
							</div>
							
							<div class='col-sm-4'>
								<button type="button" class="btn btn-success">+</button><br>
								Synonyms1: <input type="text" name="vsynonyms1" id="synonyms" class="form-control"/>
							</div>
							
							<div class='col-sm-4'>
								<button type="button" class="btn btn-success">+</button><br>
								Antonyms1: <input type="text" name="vantonyms1" id="antonyms" class="form-control"/>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class='row'>
				<div class='col-md-12'>
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title">Adjective</h3>
						</div>
						<div class="panel-body">
							<div class='col-sm-4'>
								<button type="button" class="btn btn-info">+</button><br>
								Translate1: <input type="text" name="vtranslate1" id="translate" class="form-control"/>
							</div>
							
							<div class='col-sm-4'>
								<button type="button" class="btn btn-info">+</button><br>
								Synonyms1: <input type="text" name="vsynonyms1" id="synonyms" class="form-control"/>
							</div>
							
							<div class='col-sm-4'>
								<button type="button" class="btn btn-info">+</button><br>
								Antonyms1: <input type="text" name="vantonyms1" id="antonyms" class="form-control"/>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
		</div>
			<!--
			<center><h4><b>Vocabulary:</b></h4> <input type="text" name="vocab" id="vocab" /><br /></center>
			<h4><b><center>Verb</center></b></h4>
			<center>Translate1: <input type="text" name="vtranslate1" id="translate" />
			Translate2: <input type="text" name="vtranslate2" id="translate" />
			Translate3: <input type="text" name="vtranslate3" id="translate" /><br />
			Synonyms1: <input type="text" name="vsynonyms1" id="synonyms" />
			Synonyms2: <input type="text" name="vsynonyms2" id="synonyms" />
			Synonyms3: <input type="text" name="vsynonyms3" id="synonyms" /><br />
			Antonyms1: <input type="text" name="vantonyms1" id="antonyms" />
			Antonyms2: <input type="text" name="vantonyms2" id="antonyms" />
			Antonyms3: <input type="text" name="vantonyms3" id="antonyms" /></center>
			<h4><b><center>Noun</center></b></h4>
			<center>Translate1: <input type="text" name="ntranslate1" id="translate" />
			Translate2: <input type="text" name="ntranslate2" id="translate" />
			Translate3: <input type="text" name="ntranslate3" id="translate" /><br />
			Synonyms1: <input type="text" name="nsynonyms1" id="synonyms" />
			Synonyms2: <input type="text" name="nsynonyms2" id="synonyms" />
			Synonyms3: <input type="text" name="nsynonyms3" id="synonyms" /><br />
			Antonyms1: <input type="text" name="nantonyms1" id="antonyms" />
			Antonyms2: <input type="text" name="nantonyms2" id="antonyms" />
			Antonyms3: <input type="text" name="nantonyms3" id="antonyms" /></center>
			<h4><b><center>Adjective</center></b></h4>
			<center>Translate1: <input type="text" name="adjtranslate1" id="translate" />
			Translate2: <input type="text" name="adjtranslate2" id="translate" />
			Translate3: <input type="text" name="adjtranslate3" id="translate" /><br />
			Synonyms1: <input type="text" name="adjsynonyms1" id="synonyms" />
			Synonyms2: <input type="text" name="adjsynonyms2" id="synonyms" />
			Synonyms3: <input type="text" name="adjsynonyms3" id="synonyms" /><br />
			Antonyms1: <input type="text" name="adjantonyms1" id="antonyms" />
			Antonyms2: <input type="text" name="adjantonyms2" id="antonyms" />
			Antonyms3: <input type="text" name="adjantonyms3" id="antonyms" /><br /></center>
			<center><input type="submit" id="submit" value="Send" /><a href='index.html'>Back to Index</a></center>
			-->
		
	</body>
	
	<!-- JS file (don't reorder the includingfile. it will cause error) -->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="js/bootstrap.min.js"></script>
</html>