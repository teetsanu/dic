var colour = {
      orange:"#EEB211",
      darkblue:"#21526a",
      purple:"#941e5e",
      limegreen:"#c1d72e",
      darkgreen:"#619b45",
      lightblue:"#009fc3",
      pink:"#d11b67",    
    }


var blow = {
    "nodes": {
        "vocab": {
            "label": "BLOW",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e1e\u0e31\u0e14",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e40\u0e1b\u0e48\u0e32",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e1e\u0e48\u0e19\u0e25\u0e21",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "flurry",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "gale",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "gust",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "calm",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "fortune",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "luck",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e25\u0e21\u0e41\u0e23\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e01\u0e32\u0e23\u0e40\u0e1b\u0e48\u0e32",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var long = {
    "nodes": {
        "vocab": {
            "label": "LONG",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e1b\u0e23\u0e32\u0e23\u0e16\u0e19\u0e32",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e2d\u0e22\u0e32\u0e01\u0e44\u0e14\u0e49",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "wish",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "yearn",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "desire",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "despise",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "hate",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "dislike",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e01\u0e32\u0e07\u0e40\u0e01\u0e07\u0e02\u0e32\u0e22\u0e32\u0e27",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e40\u0e27\u0e25\u0e32\u0e19\u0e32\u0e19",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "\u0e22\u0e32\u0e27",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "\u0e22\u0e32\u0e27\u0e19\u0e32\u0e19",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": " \u0e22\u0e37\u0e14\u0e22\u0e32\u0e27",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "lengthy",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "longish",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "extended",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "short",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "brief",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var way = {
    "nodes": {
        "vocab": {
            "label": "WAY",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e17\u0e48\u0e32\u0e17\u0e32\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e40\u0e2a\u0e49\u0e19\u0e17\u0e32\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e27\u0e34\u0e18\u0e35",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "use",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "mode",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "action",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "inaction",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "end",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "inactivity",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var want = {
    "nodes": {
        "vocab": {
            "label": "WANT",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e15\u0e49\u0e2d\u0e07\u0e01\u0e32\u0e23",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": " \u0e23\u0e39\u0e49\u0e2a\u0e36\u0e01\u0e02\u0e32\u0e14(\u0e1a\u0e32\u0e07\u0e2a\u0e34\u0e48\u0e07)\u0e44\u0e1b",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e1b\u0e23\u0e32\u0e23\u0e16\u0e19\u0e32",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "desire",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "require",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "desiderate",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "loathe",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "dislike",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "hate",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e22\u0e32\u0e01\u0e08\u0e19",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e02\u0e32\u0e14\u0e41\u0e04\u0e25\u0e19",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e1b\u0e23\u0e32\u0e23\u0e16\u0e19\u0e32",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "poverty",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "need",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "hunger",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "plenty",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "distaste",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "wealth",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var prove = {
    "nodes": {
        "vocab": {
            "label": "PROVE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e1e\u0e34\u0e2a\u0e39\u0e08\u0e19\u0e4c",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e15\u0e23\u0e27\u0e08\u0e2a\u0e2d\u0e1a",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e1b\u0e23\u0e32\u0e01\u0e0e\u0e04\u0e27\u0e32\u0e21\u0e08\u0e23\u0e34\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "confirm",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "test",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "fix",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "conceal",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "refute",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "hide",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var help = {
    "nodes": {
        "vocab": {
            "label": "HELP",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e0a\u0e48\u0e27\u0e22",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e43\u0e2b\u0e49\u0e04\u0e27\u0e32\u0e21\u0e0a\u0e48\u0e27\u0e22\u0e40\u0e2b\u0e25\u0e37\u0e2d",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e2a\u0e48\u0e07\u0e40\u0e2a\u0e23\u0e34\u0e21",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "aid",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "assist",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "succor",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "obstruct",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "hold back",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "hinder",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e01\u0e32\u0e23\u0e0a\u0e48\u0e27\u0e22",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e04\u0e19\u0e17\u0e35\u0e48\u0e0a\u0e48\u0e27\u0e22\u0e40\u0e2b\u0e25\u0e37\u0e2d",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "advice",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "hand",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "lift",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "stop",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "disease",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "hurt",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var hide = {
    "nodes": {
        "vocab": {
            "label": "HIDE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e0b\u0e48\u0e2d\u0e19",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e0b\u0e38\u0e01\u0e0b\u0e48\u0e2d\u0e19",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e1b\u0e01\u0e1b\u0e34\u0e14",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "conceal",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "ensconce",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "seclude",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "disclose",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "expose",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "reveal",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e2b\u0e19\u0e31\u0e07\u0e2a\u0e31\u0e15\u0e27\u0e4c",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e2b\u0e19\u0e31\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e2b\u0e19\u0e31\u0e07\u0e04\u0e19",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "leather",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "pelt",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "skin",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": null,
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var bite = {
    "nodes": {
        "vocab": {
            "label": "BITE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e01\u0e31\u0e14",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e02\u0e1a",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e07\u0e31\u0e1a",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "chew",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "snap",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "crushstar",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "give",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "offer",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "release",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e23\u0e2d\u0e22\u0e01\u0e31\u0e14",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e2b\u0e19\u0e36\u0e48\u0e07\u0e04\u0e33\u0e2d\u0e32\u0e2b\u0e32\u0e23",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "itch",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "nip",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "sop",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "lot",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var block = {
    "nodes": {
        "vocab": {
            "label": "BLOCK",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e01\u0e35\u0e14\u0e02\u0e27\u0e32\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e2b\u0e22\u0e38\u0e14",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e02\u0e27\u0e32\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "arrest",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "haltstar",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "brake",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "advance",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "continue",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "give",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e15\u0e36\u0e01\u0e43\u0e2b\u0e0d\u0e48",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e0a\u0e48\u0e27\u0e07\u0e15\u0e36\u0e01",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e2a\u0e34\u0e48\u0e07\u0e01\u0e35\u0e14\u0e02\u0e27\u0e32\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "brick",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "section",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "cube",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "whole",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "opening",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var cool = {
    "nodes": {
        "vocab": {
            "label": "COOL",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e17\u0e31\u0e19\u0e2a\u0e21\u0e31\u0e22",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e2a\u0e07\u0e1a\u0e40\u0e22\u0e37\u0e2d\u0e01\u0e40\u0e22\u0e47\u0e19",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "abate",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "calmstar",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "freeze",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "agitate",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "extend",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "warm",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e17\u0e33\u0e43\u0e2b\u0e49\u0e40\u0e22\u0e47\u0e19\u0e25\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e17\u0e33\u0e43\u0e2b\u0e49\u0e25\u0e14\u0e19\u0e49\u0e2d\u0e22\u0e25\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "stylishness",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "calmness",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "\u0e43\u0e08\u0e40\u0e22\u0e47\u0e19",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "\u0e40\u0e22\u0e47\u0e19\u0e0a\u0e32",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "\u0e17\u0e31\u0e19\u0e2a\u0e21\u0e31\u0e22",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "chilly",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "nipping",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "unfriendly",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "hot",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "kind",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "uncool",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var face = {
    "nodes": {
        "vocab": {
            "label": "FACE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e40\u0e1c\u0e0a\u0e34\u0e0d\u0e2b\u0e19\u0e49\u0e32",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e2b\u0e31\u0e19\u0e2b\u0e19\u0e49\u0e32\u0e2d\u0e2d\u0e01\u0e44\u0e1b\u0e17\u0e32\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e21\u0e2d\u0e07\u0e2a\u0e34\u0e48\u0e07\u0e2b\u0e19\u0e36\u0e48\u0e07\u0e2a\u0e34\u0e48\u0e07\u0e43\u0e14",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "confront",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "risk",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "stand",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "avoid",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "hold",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "hide",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e43\u0e1a\u0e2b\u0e19\u0e49\u0e32",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e0a\u0e37\u0e48\u0e2d\u0e40\u0e2a\u0e35\u0e22\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e23\u0e39\u0e1b\u0e25\u0e31\u0e01\u0e29\u0e13\u0e4c",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "look",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "surface",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "reputation",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "grin",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "behind",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "rear",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var mean = {
    "nodes": {
        "vocab": {
            "label": "MEAN",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e2b\u0e21\u0e32\u0e22\u0e04\u0e27\u0e32\u0e21\u0e27\u0e48\u0e32",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e21\u0e35\u0e40\u0e08\u0e15\u0e19\u0e32",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e15\u0e31\u0e49\u0e07\u0e43\u0e08",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "signify",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "intend",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "spell",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "ignore",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "deny",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "refuse",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e04\u0e48\u0e32\u0e40\u0e09\u0e25\u0e35\u0e48\u0e22",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e15\u0e31\u0e27\u0e01\u0e25\u0e32\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e21\u0e31\u0e0a\u0e0c\u0e34\u0e21",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "norm",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "balance",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "center",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "border",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "margin",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "outside",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "\u0e43\u0e08\u0e23\u0e49\u0e32\u0e22",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "\u0e17\u0e23\u0e38\u0e14\u0e42\u0e17\u0e23\u0e21",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "\u0e43\u0e08\u0e41\u0e04\u0e1a",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "greedy",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "selfish",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "close",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "polite",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "kind",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "nice",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var can = {
    "nodes": {
        "vocab": {
            "label": "CAN",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e1a\u0e23\u0e23\u0e08\u0e38\u0e01\u0e23\u0e30\u0e1b\u0e4b\u0e2d\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e2a\u0e32\u0e21\u0e32\u0e23\u0e16",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e22\u0e34\u0e19\u0e22\u0e2d\u0e21",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "commit",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "may",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "allow",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "cannot",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "employ",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "hire",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e01\u0e23\u0e30\u0e1b\u0e4b\u0e2d\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e04\u0e38\u0e01(\u0e04\u0e33\u0e2a\u0e41\u0e25\u0e07)",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e1b\u0e23\u0e34\u0e21\u0e32\u0e13\u0e2b\u0e19\u0e36\u0e48\u0e07\u0e01\u0e23\u0e30\u0e1b\u0e4b\u0e2d\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "bottle",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "canful",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "tin",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "front",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var cure = {
    "nodes": {
        "vocab": {
            "label": "CURE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e16\u0e19\u0e2d\u0e21\u0e2d\u0e32\u0e2b\u0e32\u0e23",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e41\u0e01\u0e49\u0e44\u0e02\u0e1b\u0e31\u0e0d\u0e2b\u0e32",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e23\u0e31\u0e01\u0e29\u0e32",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "restore",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "preserve",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "keep",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "ruin",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "harm",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "soften",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e01\u0e23\u0e30\u0e1a\u0e27\u0e19\u0e01\u0e32\u0e23\u0e16\u0e19\u0e2d\u0e21\u0e2d\u0e32\u0e2b\u0e32\u0e23",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e01\u0e32\u0e23\u0e41\u0e01\u0e49\u0e44\u0e02\u0e1b\u0e31\u0e0d\u0e2b\u0e32",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e01\u0e32\u0e23\u0e1f\u0e37\u0e49\u0e19\u0e1f\u0e39",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "recovery",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "treatment",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "medicine",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "injury",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "hurt",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "problem",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var tense = {
    "nodes": {
        "vocab": {
            "label": "TENSE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e17\u0e33\u0e43\u0e2b\u0e49\u0e15\u0e36\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e15\u0e36\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "stress",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "worriedstar",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "uneasystar",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "happy",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "limp",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "slack",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e01\u0e32\u0e25",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e01\u0e23\u0e34\u0e22\u0e32\u0e41\u0e2a\u0e14\u0e07\u0e40\u0e27\u0e25\u0e32",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "\u0e15\u0e36\u0e07\u0e40\u0e04\u0e23\u0e35\u0e22\u0e14",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "\u0e40\u0e04\u0e23\u0e48\u0e07\u0e40\u0e04\u0e23\u0e35\u0e22\u0e14",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "\u0e41\u0e19\u0e48\u0e19",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "stretched",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "tight",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "exciteds",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "relaxed",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "flexible",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "calm",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var allow = {
    "nodes": {
        "vocab": {
            "label": "ALLOW",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e41\u0e1a\u0e48\u0e07\u0e2a\u0e23\u0e23",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e22\u0e2d\u0e21\u0e23\u0e31\u0e1a",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e2d\u0e19\u0e38\u0e0d\u0e32\u0e15",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "grant",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "approve",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "assign",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "keep",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "deny",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "refuse",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var pick = {
    "nodes": {
        "vocab": {
            "label": "PICK",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e40\u0e14\u0e47\u0e14",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e40\u0e25\u0e37\u0e2d\u0e01",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e2b\u0e32\u0e40\u0e23\u0e37\u0e48\u0e2d\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "choice",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "pull",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "crack",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "grow",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "push",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "ignore",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var dust = {
    "nodes": {
        "vocab": {
            "label": "DUST",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e1b\u0e31\u0e14\u0e1d\u0e38\u0e48\u0e19",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e01\u0e23\u0e30\u0e08\u0e32\u0e22",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "vaccuum",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "sweep",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "whisk",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "collect",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "gather",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e1d\u0e38\u0e48\u0e19",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e25\u0e30\u0e2d\u0e2d\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e40\u0e28\u0e29\u0e1c\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "dirt",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "mote",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "powder",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "cleanliness",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "purity",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var coach = {
    "nodes": {
        "vocab": {
            "label": "COACH",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e1d\u0e36\u0e01\u0e01\u0e35\u0e2c\u0e32\u0e43\u0e2b\u0e49",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e17\u0e33\u0e2b\u0e19\u0e49\u0e32\u0e17\u0e35\u0e48\u0e40\u0e1b\u0e47\u0e19\u0e04\u0e23\u0e39\u0e1d\u0e36\u0e01",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "teach",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "instruct",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "drill",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "listen",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "learn",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "accept",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e04\u0e23\u0e39\u0e1e\u0e34\u0e40\u0e28\u0e29",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e04\u0e23\u0e39\u0e1d\u0e36\u0e01\u0e01\u0e35\u0e2c\u0e32",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e23\u0e16\u0e22\u0e19\u0e15\u0e4c\u0e02\u0e19\u0e32\u0e14\u0e40\u0e25\u0e47\u0e01\u0e23\u0e32\u0e04\u0e32\u0e16\u0e39\u0e01",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "carriage",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "tutor",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "teacher",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "student",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "player",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "pupil",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var produce = {
    "nodes": {
        "vocab": {
            "label": "PRODUCE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e2a\u0e23\u0e49\u0e32\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e1c\u0e25\u0e34\u0e15",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e17\u0e33\u0e43\u0e2b\u0e49\u0e21\u0e35\u0e02\u0e36\u0e49\u0e19",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "generate",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "deliver",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "crop",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "stop",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "ruin",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "halt",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e1c\u0e25\u0e34\u0e15\u0e1c\u0e25",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e1c\u0e25\u0e1c\u0e25\u0e34\u0e15",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "result",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "product",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var poke = {
    "nodes": {
        "vocab": {
            "label": "POKE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e01\u0e32\u0e23\u0e17\u0e34\u0e48\u0e21",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e01\u0e23\u0e30\u0e40\u0e1b\u0e4b\u0e32\u0e2a\u0e15\u0e32\u0e07\u0e04\u0e4c",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e01\u0e32\u0e23\u0e14\u0e31\u0e19",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "jab",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "punch",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "wallet",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e40\u0e02\u0e49\u0e32\u0e44\u0e1b\u0e22\u0e38\u0e48\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e41\u0e2b\u0e22\u0e48",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e42\u0e1c\u0e25\u0e48\u0e2d\u0e2d\u0e01\u0e21\u0e32",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "dig",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "peek",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "shove",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "depress",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "rush",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "calm",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var fire = {
    "nodes": {
        "vocab": {
            "label": "FIRE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e08\u0e38\u0e14\u0e44\u0e1f",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e22\u0e34\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e44\u0e25\u0e48\u0e2d\u0e2d\u0e01",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "set on fire",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "shoot",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "dismiss",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "bore",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "hold",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "employ",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e40\u0e1e\u0e25\u0e34\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e01\u0e32\u0e23\u0e22\u0e34\u0e07\u0e1b\u0e37\u0e19\u0e1e\u0e23\u0e49\u0e2d\u0e21\u0e46\u0e01\u0e31\u0e19",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e01\u0e2d\u0e07\u0e44\u0e1f",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "flames",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "blaze",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "volley",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "apathy",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "calm",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "coldness",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var cut = {
    "nodes": {
        "vocab": {
            "label": "CUT",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e08\u0e49\u0e27\u0e07\u0e41\u0e17\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e15\u0e31\u0e14",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e04\u0e21\u0e1e\u0e2d (\u0e17\u0e35\u0e48\u0e08\u0e30\u0e15\u0e31\u0e14\u0e44\u0e14\u0e49)",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "carve",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "slice",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "clip",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "combine",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "unite",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "close",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e01\u0e32\u0e23\u0e02\u0e32\u0e14\u0e40\u0e23\u0e35\u0e22\u0e19",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e01\u0e32\u0e23\u0e25\u0e14\u0e25\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e09\u0e1a\u0e31\u0e1a\u0e15\u0e31\u0e14\u0e15\u0e48\u0e2d (\u0e20\u0e32\u0e1e\u0e22\u0e19\u0e15\u0e23\u0e4c)",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "reduction",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "fall",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "increase",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "uncut",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var yard = {
    "nodes": {
        "vocab": {
            "label": "YARD",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e2a\u0e19\u0e32\u0e21",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e04\u0e32\u0e19\u0e22\u0e32\u0e27\u0e1e\u0e22\u0e38\u0e07\u0e43\u0e1a\u0e40\u0e23\u0e37\u0e2d",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e25\u0e32\u0e19\u0e1a\u0e49\u0e32\u0e19",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "backyard",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "playground",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "garden",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var stop = {
    "nodes": {
        "vocab": {
            "label": "STOP",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e2b\u0e22\u0e38\u0e14",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e23\u0e30\u0e07\u0e31\u0e1a",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e1b\u0e34\u0e14\u0e01\u0e31\u0e49\u0e19",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "pause",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "prevent",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "block",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "go",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "start",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "continuation",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e01\u0e32\u0e23\u0e2b\u0e22\u0e38\u0e14",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e01\u0e32\u0e23\u0e41\u0e27\u0e30\u0e1e\u0e31\u0e01",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e2a\u0e34\u0e48\u0e07\u0e01\u0e35\u0e14\u0e02\u0e27\u0e32\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "blockage",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "stay",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "end",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "beginning",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "opening",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "aid",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var care = {
    "nodes": {
        "vocab": {
            "label": "CARE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e14\u0e39\u0e41\u0e25",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e2b\u0e48\u0e27\u0e07\u0e43\u0e22",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e1b\u0e01\u0e1b\u0e49\u0e2d\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "concern",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "protectstar",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "tend",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "ignore",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "forget",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "hurt",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e01\u0e32\u0e23\u0e14\u0e39\u0e41\u0e25",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e23\u0e30\u0e21\u0e31\u0e14\u0e23\u0e30\u0e27\u0e31\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e2b\u0e48\u0e27\u0e07\u0e43\u0e22",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "effort",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "effort",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "interest",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "apathy",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "carelessness",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "oversight",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var change = {
    "nodes": {
        "vocab": {
            "label": "CHANGE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e17\u0e33\u0e43\u0e2b\u0e49\u0e41\u0e15\u0e01\u0e15\u0e48\u0e32\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e40\u0e1b\u0e25\u0e35\u0e48\u0e22\u0e19",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e40\u0e1b\u0e25\u0e35\u0e48\u0e22\u0e19\u0e04\u0e27\u0e32\u0e21\u0e40\u0e23\u0e47\u0e27",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "convert",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "modify",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "shift",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "expand",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "stay",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "keep",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e40\u0e07\u0e34\u0e19\u0e17\u0e2d\u0e19",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e01\u0e32\u0e23\u0e41\u0e17\u0e19\u0e17\u0e35\u0e48",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e40\u0e1b\u0e25\u0e35\u0e48\u0e22\u0e19\u0e41\u0e1b\u0e25\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "supplanting",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "novelty",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "switch",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "remain",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "agreement",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "bill",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var respect = {
    "nodes": {
        "vocab": {
            "label": "RESPECT",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e40\u0e04\u0e32\u0e23\u0e1e",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e40\u0e2d\u0e32\u0e43\u0e08\u0e43\u0e2a\u0e48",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e19\u0e31\u0e1a\u0e16\u0e37\u0e2d",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "revere",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "regard",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "follow",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "disregard",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "hate",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "ignore",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e1b\u0e23\u0e30\u0e40\u0e14\u0e47\u0e19",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e40\u0e04\u0e32\u0e23\u0e1e",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e40\u0e2d\u0e32\u0e43\u0e08\u0e43\u0e2a\u0e48",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "honour",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "consideration",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "point",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "dishonor",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "disrespect",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "bad manners",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var harm = {
    "nodes": {
        "vocab": {
            "label": "HARM",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e17\u0e33\u0e23\u0e49\u0e32\u0e22",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e17\u0e33\u0e2d\u0e31\u0e19\u0e15\u0e23\u0e32\u0e22",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e17\u0e33\u0e43\u0e2b\u0e49\u0e44\u0e14\u0e49\u0e23\u0e31\u0e1a\u0e1a\u0e32\u0e14\u0e40\u0e08\u0e47\u0e1a",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "abuse",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "crush",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "wrong",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "aid",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "cure",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "heal",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e0a\u0e31\u0e48\u0e27\u0e23\u0e49\u0e32\u0e22",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e40\u0e2a\u0e35\u0e22\u0e2b\u0e32\u0e22",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e01\u0e32\u0e23\u0e1a\u0e32\u0e14\u0e40\u0e08\u0e47\u0e1a",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "injury",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "damage",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "violence",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "favor",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "good",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "kindness",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var good = {
    "nodes": {
        "vocab": {
            "label": "GOOD",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e14\u0e35\u0e07\u0e32\u0e21",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e1c\u0e25\u0e1b\u0e23\u0e30\u0e42\u0e22\u0e0a\u0e19\u0e4c",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e04\u0e38\u0e13\u0e18\u0e23\u0e23\u0e21",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "goodness",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "righteousness",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "benefit",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "bad fortune",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "sin",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "wrong",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "\u0e14\u0e35",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "\u0e40\u0e2b\u0e21\u0e32\u0e30\u0e2a\u0e21",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "\u0e40\u0e01\u0e48\u0e07",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "suitable",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "acceptable",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "fine",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "evil",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "fake",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "mean",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var bad = {
    "nodes": {
        "vocab": {
            "label": "BAD",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "\u0e40\u0e25\u0e27",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "\u0e15\u0e48\u0e33\u0e01\u0e27\u0e48\u0e32\u0e21\u0e32\u0e15\u0e23\u0e10\u0e32\u0e19",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "\u0e44\u0e21\u0e48\u0e21\u0e35\u0e04\u0e27\u0e32\u0e21\u0e2a\u0e38\u0e02",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "poor quality",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "harmful",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "wrong",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "moral",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "good",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "pleasant",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var perfect = {
    "nodes": {
        "vocab": {
            "label": "PERFECT",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e17\u0e33\u0e43\u0e2b\u0e49\u0e2a\u0e21\u0e1a\u0e39\u0e23\u0e13\u0e4c",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e40\u0e23\u0e35\u0e22\u0e1a\u0e23\u0e49\u0e2d\u0e22",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e2a\u0e21\u0e1a\u0e39\u0e23\u0e13\u0e4c\u0e41\u0e1a\u0e1a",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "complete",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "improve",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "achieve",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "imperfect",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "ruin",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "fail",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e01\u0e32\u0e25\u0e2a\u0e21\u0e1a\u0e39\u0e23\u0e13\u0e4c (\u0e17\u0e32\u0e07\u0e44\u0e27\u0e22\u0e32\u0e01\u0e23\u0e13\u0e4c)",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "\u0e40\u0e2b\u0e21\u0e32\u0e30\u0e2a\u0e21",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "\u0e1a\u0e23\u0e34\u0e2a\u0e38\u0e17\u0e18\u0e34\u0e4c",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "\u0e40\u0e0a\u0e35\u0e48\u0e22\u0e27\u0e0a\u0e32\u0e0d",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "excellent",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "expert",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "pure",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "imperfect",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "bad",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "false",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var toxic = {
    "nodes": {
        "vocab": {
            "label": "TOXIC",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "\u0e21\u0e35\u0e1e\u0e34\u0e29",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "\u0e01\u0e34\u0e19\u0e44\u0e21\u0e48\u0e44\u0e14\u0e49",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "\u0e40\u0e1b\u0e47\u0e19\u0e1e\u0e34\u0e29",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "poisonous",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "deadly",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "harmful",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "harmless",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "helpful",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "healthy",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var inhibit = {
    "nodes": {
        "vocab": {
            "label": "INHIBIT",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e22\u0e31\u0e1a\u0e22\u0e31\u0e49\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e01\u0e31\u0e49\u0e19\u0e02\u0e27\u0e32\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e02\u0e31\u0e14\u0e02\u0e27\u0e32\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "constrain",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "prohibit",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "repress",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "advance",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "encourage",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "help",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var work = {
    "nodes": {
        "vocab": {
            "label": "WORK",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e44\u0e14\u0e49\u0e1c\u0e25",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e40\u0e1b\u0e47\u0e19\u0e1c\u0e25\u0e2a\u0e33\u0e40\u0e23\u0e47\u0e08",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e43\u0e0a\u0e49\u0e01\u0e32\u0e23\u0e44\u0e14\u0e49",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "function",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "operate",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "achieve",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "abstain",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "give up",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "pause",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e07\u0e32\u0e19",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e17\u0e35\u0e48\u0e17\u0e33\u0e07\u0e32\u0e19",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e1e\u0e22\u0e32\u0e22\u0e32\u0e21\u0e17\u0e35\u0e48\u0e43\u0e0a\u0e49\u0e44\u0e1b\u0e43\u0e19\u0e01\u0e32\u0e23\u0e17\u0e33\u0e07\u0e32\u0e19",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "effort",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "occupation",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "career",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "entertainment",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "unemployment",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "laziness",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var lead = {
    "nodes": {
        "vocab": {
            "label": "LEAD",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e01\u0e48\u0e2d\u0e43\u0e2b\u0e49\u0e40\u0e01\u0e34\u0e14",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e04\u0e27\u0e1a\u0e04\u0e38\u0e21",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e43\u0e0a\u0e49\u0e0a\u0e35\u0e27\u0e34\u0e15",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "cause",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "command",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "live",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "fail",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "take",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "lose",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e15\u0e31\u0e27\u0e40\u0e2d\u0e01",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e02\u0e49\u0e2d\u0e21\u0e39\u0e25\u0e17\u0e35\u0e48\u0e40\u0e1b\u0e47\u0e19\u0e1b\u0e23\u0e30\u0e42\u0e22\u0e0a\u0e19\u0e4c\u0e43\u0e19\u0e01\u0e32\u0e23\u0e04\u0e49\u0e19\u0e2b\u0e32",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e15\u0e33\u0e41\u0e2b\u0e19\u0e48\u0e07\u0e19\u0e33",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "first position",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "tip",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "top",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "center",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "rear",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "last",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var value = {
    "nodes": {
        "vocab": {
            "label": "VALUE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e1b\u0e23\u0e30\u0e40\u0e21\u0e34\u0e19\u0e04\u0e48\u0e32",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e43\u0e2b\u0e49\u0e04\u0e27\u0e32\u0e21\u0e2a\u0e33\u0e04\u0e31\u0e0d",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e19\u0e31\u0e1a\u0e16\u0e37\u0e2d\u0e2d\u0e22\u0e48\u0e32\u0e07\u0e2a\u0e39\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "rate",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "respect",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "evaluate",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "detriment",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "disadvantage",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e21\u0e39\u0e25\u0e04\u0e48\u0e32",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e04\u0e38\u0e13\u0e04\u0e48\u0e32",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e2a\u0e33\u0e04\u0e31\u0e0d",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "worth",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "goodness",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "merit",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "uselessness",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "worthlessness",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "unfitness",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var effect = {
    "nodes": {
        "vocab": {
            "label": "EFFECT",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e01\u0e48\u0e2d\u0e43\u0e2b\u0e49\u0e40\u0e01\u0e34\u0e14",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e2d\u0e2d\u0e01\u0e24\u0e17\u0e18\u0e34\u0e4c",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e1a\u0e31\u0e07\u0e40\u0e01\u0e34\u0e14\u0e1c\u0e25",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "bring ",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "about",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "cause",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "fail",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "stop",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "discourage",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e15\u0e31\u0e49\u0e07\u0e43\u0e08",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e23\u0e39\u0e49\u0e2a\u0e36\u0e01\u0e17\u0e35\u0e48\u0e40\u0e01\u0e34\u0e14\u0e02\u0e36\u0e49\u0e19",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e1c\u0e25\u0e01\u0e23\u0e30\u0e17\u0e1a",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "outcome",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "intention",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "reaction",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "beginning",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "origin",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "start",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var pain = {
    "nodes": {
        "vocab": {
            "label": "PAIN",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e17\u0e33\u0e43\u0e2b\u0e49\u0e40\u0e2a\u0e35\u0e22\u0e43\u0e08",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e23\u0e39\u0e49\u0e2a\u0e36\u0e01\u0e40\u0e08\u0e47\u0e1a",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e17\u0e33\u0e43\u0e2b\u0e49\u0e40\u0e08\u0e47\u0e1a\u0e1b\u0e27\u0e14",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "burn",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "hurt",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "agonize",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "assuge",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "ease",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "calm",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e40\u0e08\u0e47\u0e1a\u0e1b\u0e27\u0e14",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e17\u0e38\u0e01\u0e02\u0e4c",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e40\u0e28\u0e23\u0e49\u0e32",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "ache",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "discomfort",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "sorrow",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "pleasure",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "joy",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "aid",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var request = {
    "nodes": {
        "vocab": {
            "label": "REQUEST",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e02\u0e2d\u0e23\u0e49\u0e2d\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e40\u0e23\u0e35\u0e22\u0e01\u0e23\u0e49\u0e2d\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e02\u0e2d",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "ask for ",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "demand ",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "desire",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "refuse",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "give",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "reply",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e15\u0e49\u0e2d\u0e07\u0e01\u0e32\u0e23",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e01\u0e32\u0e23\u0e02\u0e2d\u0e23\u0e49\u0e2d\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e01\u0e32\u0e23\u0e40\u0e23\u0e35\u0e22\u0e01\u0e23\u0e49\u0e2d\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "requirement",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "asking ",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "appeal",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "refusal",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "reply",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "answer",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var soft = {
    "nodes": {
        "vocab": {
            "label": "SOFT",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e19\u0e34\u0e48\u0e21",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "\u0e2d\u0e48\u0e2d\u0e19\u0e42\u0e22\u0e19",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "\u0e19\u0e38\u0e48\u0e21\u0e19\u0e27\u0e25",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "\u0e1c\u0e48\u0e2d\u0e19\u0e1b\u0e23\u0e19",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "considerate",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "delicate",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "smooth",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "rough",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "coarse",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "stubborn",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var contain = {
    "nodes": {
        "vocab": {
            "label": "CONTAIN",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e08\u0e33\u0e01\u0e31\u0e14",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e22\u0e31\u0e1a\u0e22\u0e31\u0e49\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e1b\u0e23\u0e30\u0e01\u0e2d\u0e1a\u0e14\u0e49\u0e27\u0e22",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "carry ",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "hold",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "include",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "exclude",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "release",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "continue",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var hope = {
    "nodes": {
        "vocab": {
            "label": "HOPE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e2b\u0e27\u0e31\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e1b\u0e23\u0e32\u0e23\u0e16\u0e19\u0e32",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e04\u0e32\u0e14",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "expect",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "wish",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "want",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "abandon",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "distrust",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "despair",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e04\u0e27\u0e32\u0e21\u0e2b\u0e27\u0e31\u0e07",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "faith",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "reward",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "dream",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "disbelief",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "doubt",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "hatred",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var stitch = {
    "nodes": {
        "vocab": {
            "label": "STITCH",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e40\u0e22\u0e47\u0e1a",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "seam ",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "sew",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "baste",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e23\u0e2d\u0e22\u0e40\u0e22\u0e47\u0e1a",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e15\u0e30\u0e40\u0e02\u0e47\u0e1a",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "basting",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "suture",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var apologise = {
    "nodes": {
        "vocab": {
            "label": "APOLOGISE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e41\u0e01\u0e49\u0e15\u0e48\u0e32\u0e07",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e02\u0e2d\u0e42\u0e17\u0e29",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "\u0e41\u0e01\u0e49\u0e15\u0e31\u0e27",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "apologise",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "atone",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "purge",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "defy",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var habit = {
    "nodes": {
        "vocab": {
            "label": "HABIT",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e19\u0e34\u0e2a\u0e31\u0e22",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e01\u0e34\u0e08\u0e27\u0e31\u0e15\u0e23",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e1b\u0e23\u0e30\u0e40\u0e1e\u0e13\u0e35",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "custom",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "addiction",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "routine",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "disinclination",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "hate",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "dislike",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var rod = {
    "nodes": {
        "vocab": {
            "label": "ROD",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "\u0e04\u0e31\u0e19\u0e40\u0e1a\u0e47\u0e14",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "\u0e44\u0e21\u0e49\u0e40\u0e23\u0e35\u0e22\u0e27",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "\u0e1b\u0e37\u0e19\u0e40\u0e25\u0e47\u0e01",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "whipping stick",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "fishing rod",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "pistol",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var slow = {
    "nodes": {
        "vocab": {
            "label": "SLOW",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "\u0e1d\u0e37\u0e14\u0e40\u0e04\u0e37\u0e2d\u0e07 (\u0e17\u0e32\u0e07\u0e18\u0e38\u0e23\u0e01\u0e34\u0e08)",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "\u0e44\u0e21\u0e48\u0e04\u0e25\u0e48\u0e2d\u0e07 (\u0e17\u0e32\u0e07\u0e18\u0e38\u0e23\u0e01\u0e34\u0e08)",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "delay ",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "restrict",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "abate",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "advance",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "encourage",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "forward",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "\u0e0a\u0e49\u0e32",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "\u0e17\u0e35\u0e48\u0e43\u0e0a\u0e49\u0e40\u0e27\u0e25\u0e32\u0e19\u0e32\u0e19",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "\u0e2d\u0e37\u0e14\u0e2d\u0e32\u0e14",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "sluggish",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "easy",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "heavy",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "active",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "enthusiastic",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "busy",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var busy = {
    "nodes": {
        "vocab": {
            "label": "BUSY",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "\u0e22\u0e38\u0e48\u0e07\u0e27\u0e38\u0e48\u0e19\u0e27\u0e32\u0e22",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "\u0e02\u0e22\u0e31\u0e19",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "\u0e2a\u0e31\u0e1a\u0e2a\u0e19",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "active",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "bustling",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "curious",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "idle",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "lazy",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "unbusy",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var idle = {
    "nodes": {
        "vocab": {
            "label": "IDLE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "\u0e40\u0e01\u0e35\u0e22\u0e08\u0e04\u0e23\u0e49\u0e32\u0e19",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "\u0e0b\u0e36\u0e48\u0e07\u0e22\u0e31\u0e07\u0e44\u0e21\u0e48\u0e44\u0e14\u0e49\u0e43\u0e0a\u0e49\u0e01\u0e32\u0e23",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "\u0e02\u0e35\u0e49\u0e40\u0e01\u0e35\u0e22\u0e08",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "empty",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "worthless ",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "ineffective",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "active",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "diligent",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "working",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}


var abc = {
    "nodes": {
        "vocab": {
            "label": "ABC",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "dsads",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "jhj",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "jhjh",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "jhjhj",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "jhjhj",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "hjhjh",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "jhjhj",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "jhjhjhj",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "jhjhj",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "jhjhj",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "jhjhj",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "jhjhj",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "jhj",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "jhjhj",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "jhjhj",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "jhjhjhj",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "jhjhj",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "kjkjkjk",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "jhjhj",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "jhjhj",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "kjkjk",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "kjkjkjk",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "kjkjk",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "yuiuiui",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "oiouyi",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "iuiui",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var eret = {
    "nodes": {
        "vocab": {
            "label": "ERET",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var aa = {
    "nodes": {
        "vocab": {
            "label": "AA",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "q",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": [
            "vtranslate3: []"
        ],
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var bb = {
    "nodes": {
        "vocab": {
            "label": "BB",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": [
            null
        ],
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var dd = {
    "nodes": {
        "vocab": {
            "label": "DD",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": [
            
        ],
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var ee = {
    "nodes": {
        "vocab": {
            "label": "EE",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": [
            ""
        ],
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var ff = {
    "nodes": {
        "vocab": {
            "label": "FF",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "1234",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "asdf",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": [
            "vtranslate3: []"
        ],
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var gg = {
    "nodes": {
        "vocab": {
            "label": "GG",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "wef",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "asdf",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": [
            "vtranslate3: []"
        ],
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var hh = {
    "nodes": {
        "vocab": {
            "label": "HH",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "qewr",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "qwer",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": [
            "vtranslate3\": []"
        ],
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var ii = {
    "nodes": {
        "vocab": {
            "label": "II",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "qewr",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "qwer",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var blow = {
    "nodes": {
        "vocab": {
            "label": "BLOW",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var Blow = {
    "nodes": {
        "vocab": {
            "label": "BLOW",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var JJ = {
    "nodes": {
        "vocab": {
            "label": "JJ",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var KK = {
    "nodes": {
        "vocab": {
            "label": "KK",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var ll = {
    "nodes": {
        "vocab": {
            "label": "LL",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}

var kk = {
    "nodes": {
        "vocab": {
            "label": "KK",
            "color": "#EEB211",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "verb": {
            "label": "Verb",
            "color": "#21526a",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "noun": {
            "label": "Noun",
            "color": "#941e5e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "adj": {
            "label": "Adj",
            "color": "#c1d72e",
            "shape": "dot",
            "radius": 60,
            "alpha": 1
        },
        "vtranslate": {
            "label": "Translate",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vsynonyms": {
            "label": "Synonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vantonyms": {
            "label": "Antonyms",
            "color": "#21526a",
            "shape": "dot",
            "alpha": 1
        },
        "vtranslate1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vtranslate3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vsynonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms1": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms2": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "vantonyms3": {
            "label": "-",
            "color": "#21526a",
            "alpha": 0
        },
        "ntranslate": {
            "label": "Translate",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nsynonyms": {
            "label": "Synonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "nantonyms": {
            "label": "Antonyms",
            "color": "#941e5e",
            "shape": "dot",
            "alpha": 1
        },
        "ntranslate1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "ntranslate3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nsynonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms1": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms2": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "nantonyms3": {
            "label": "-",
            "color": "#941e5e",
            "alpha": 0
        },
        "adjtranslate": {
            "label": "Translate",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjsynonyms": {
            "label": "Synonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjantonyms": {
            "label": "Antonyms",
            "color": "#c1d72e",
            "shape": "dot",
            "alpha": 1
        },
        "adjtranslate1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjtranslate3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjsynonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms1": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms2": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        },
        "adjantonyms3": {
            "label": "-",
            "color": "#c1d72e",
            "alpha": 0
        }
    },
    "edges": {
        "vocab": {
            "verb": [],
            "noun": [],
            "adj": []
        },
        "verb": {
            "vtranslate": [],
            "vsynonyms": [],
            "vantonyms": []
        },
        "vtranslate": {
            "vtranslate1": [],
            "vtranslate2": [],
            "vtranslate3": []
        },
        "vsynonyms": {
            "vsynonyms1": [],
            "vsynonyms2": [],
            "vsynonyms3": []
        },
        "vantonyms": {
            "vantonyms1": [],
            "vantonyms2": [],
            "vantonyms3": []
        },
        "noun": {
            "ntranslate": [],
            "nsynonyms": [],
            "nantonyms": []
        },
        "ntranslate": {
            "ntranslate1": [],
            "ntranslate2": [],
            "ntranslate3": []
        },
        "nsynonyms": {
            "nsynonyms1": [],
            "nsynonyms2": [],
            "nsynonyms3": []
        },
        "nantonyms": {
            "nantonyms1": [],
            "nantonyms2": [],
            "nantonyms3": []
        },
        "adj": {
            "adjtranslate": [],
            "adjsynonyms": [],
            "adjantonyms": []
        },
        "adjtranslate": {
            "adjtranslate1": [],
            "adjtranslate2": [],
            "adjtranslate3": []
        },
        "adjsynonyms": {
            "adjsynonyms1": [],
            "adjsynonyms2": [],
            "adjsynonyms3": []
        },
        "adjantonyms": {
            "adjantonyms1": [],
            "adjantonyms2": [],
            "adjantonyms3": []
        }
    }
}