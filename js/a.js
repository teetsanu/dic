var colour = {
      orange:"#EEB211",
      darkblue:"#21526a",
      purple:"#941e5e",
      limegreen:"#c1d72e",
      darkgreen:"#619b45",
      lightblue:"#009fc3",
      pink:"#d11b67",    
    }
	var clean = {
		nodes:{
			main: {'label':'clean','color':colour.orange, 'shape':'dot', 'radius':60, 'alpha':1 },
			translate: {'label':'Translate','color':colour.darkblue,'shape':'dot','radius':30,'alpha':1 },
			synonyms: {'label':'Synonyms','color':colour.purple,'shape':'dot','radius':30,'alpha':1 },
			antonyms: {'label':'Antonyms','color':colour.limegreen,'shape':'dot','radius':30,'alpha':1 },      

			translate1:{ 'label':'1', 'color':colour.darkblue, 'alpha': 0 },    
			translate2:{ 'label':'2', 'color':colour.darkblue, 'alpha': 0 },      
			translate3:{ 'label':'3', 'color':colour.darkblue, 'alpha': 0 },  
				  
			synonyms1:{ 'label':'blank', 'color':colour.purple, 'alpha': 0 },
			synonyms2:{ 'label':'pure', 'color':colour.purple, 'alpha': 0 },
			synonyms3:{ 'label':'trim', 'color':colour.purple, 'alpha': 0 },
				  
			antonyms1:{ 'label':'dull', 'link':'d.html', 'color':colour.limegreen, 'alpha': 0 }, 
			antonyms2:{ 'label':'aware', 'color':colour.limegreen, 'alpha': 0 }, 
			antonyms3:{ 'label':'dark', 'color':colour.limegreen, 'alpha': 0 },
		},
		edges:{
			main:{ translate:{}, synonyms:{}, antonyms:{}, },
			translate:{ translate1:{}, translate2:{}, translate3:{} },
			synonyms:{ synonyms1:{}, synonyms2:{}, synonyms3:{} },
			antonyms:{ antonyms1:{}, antonyms2:{}, antonyms3:{} },
		}
    }
	var cat = {
		nodes:{
			main: {'label':'cat','color':colour.orange, 'shape':'dot', 'radius':60, 'alpha':1 },
			translate: {'label':'Translate','color':colour.darkblue,'shape':'dot','radius':30,'alpha':1 },
			synonyms: {'label':'Synonyms','color':colour.purple,'shape':'dot','radius':30,'alpha':1 },
			antonyms: {'label':'Antonyms','color':colour.limegreen,'shape':'dot','radius':30,'alpha':1 },      

			translate1:{ 'label':'1', 'color':colour.darkblue, 'alpha': 0 },    
			translate2:{ 'label':'2', 'color':colour.darkblue, 'alpha': 0 },      
			translate3:{ 'label':'3', 'color':colour.darkblue, 'alpha': 0 },  
				  
			synonyms1:{ 'label':'blank', 'color':colour.purple, 'alpha': 0 },
			synonyms2:{ 'label':'pure', 'color':colour.purple, 'alpha': 0 },
			synonyms3:{ 'label':'trim', 'color':colour.purple, 'alpha': 0 },
				  
			antonyms1:{ 'label':'dull', 'color':colour.limegreen, 'alpha': 0 }, 
			antonyms2:{ 'label':'aware', 'color':colour.limegreen, 'alpha': 0 }, 
			antonyms3:{ 'label':'dark', 'color':colour.limegreen, 'alpha': 0 },
		},
		edges:{
			main:{ translate:{}, synonyms:{}, antonyms:{} },
			translate:{ translate1:{}, translate2:{}, translate3:{} },
			synonyms:{ synonyms1:{}, synonyms2:{}, synonyms3:{} },
			antonyms:{ antonyms1:{}, antonyms2:{}, antonyms3:{} },
		}
    }
	var nong = {
		"nodes": {
			"nong": {"label": "nong", "color": "#EEB211", "shape": "dot", "radius": 60, "alpha": 1},
			"translate": {"label": "translate", "color": "#21526a", "shape": "dot", "radius": 30, "alpha": 1},
			"synonyms": {"label": "synonyms", "color": "#941e5e", "shape": "dot", "radius": 30, "alpha": 1},
			"antonyms": {"label": "antonyms", "color": "#c1d72e", "shape": "dot", "radius": 30, "alpha": 1},
        
			"translate1": {"label": "1", "color": "#21526a", "alpha": 0},
			"translate2": {"label": "2", "color": "#21526a", "alpha": 0},
			"translate3": {"label": "3", "color": "#21526a", "alpha": 0},
       
			"synonyms1": {"label": "4", "color": "#941e5e", "alpha": 0},
			"synonyms2": {"label": "5", "color": "#941e5e", "alpha": 0},
			"synonyms3": {"label": "6", "color": "#941e5e", "alpha": 0},
        
			"antonyms1": {"label": "7", "color": "#c1d72e", "alpha": 0},
			"antonyms2": {"label": "8", "color": "#c1d72e", "alpha": 0},
			"antonyms3": {"label": "9", "color": "#c1d72e", "alpha": 0},
		},
		"edges": {
			"nong": {"translate": [],"synonyms": [],"antonyms": [] },
			"translate": {"translate1": [],"translate2": [],"translate3": [] },
			"synonyms": {"synonyms1": [], "synonyms2": [], "synonyms3": [] },
			"antonyms": {"antonyms1": [],"antonyms2": [],"antonyms3": [] },
		}
	}
	var eiei = {
		"nodes": {
			"eiei": {"label": "eiei", "color": "#EEB211", "shape": "dot", "radius": 60, "alpha": 1},
			"translate": {"label": "translate", "color": "#21526a", "shape": "dot", "radius": 30, "alpha": 1},
			"synonyms": {"label": "synonyms", "color": "#941e5e", "shape": "dot", "radius": 30, "alpha": 1},
			"antonyms": {"label": "antonyms", "color": "#c1d72e", "shape": "dot", "radius": 30, "alpha": 1},
        
			"translate1": {"label": "1", "color": "#21526a", "alpha": 0},
			"translate2": {"label": "2", "color": "#21526a", "alpha": 0},
			"translate3": {"label": "3", "color": "#21526a", "alpha": 0},
       
			"synonyms1": {"label": "4", "color": "#941e5e", "alpha": 0},
			"synonyms2": {"label": "5", "color": "#941e5e", "alpha": 0},
			"synonyms3": {"label": "6", "color": "#941e5e", "alpha": 0},
        
			"antonyms1": {"label": "7", "color": "#c1d72e", "alpha": 0},
			"antonyms2": {"label": "8", "color": "#c1d72e", "alpha": 0},
			"antonyms3": {"label": "9", "color": "#c1d72e", "alpha": 0},
		},
		"edges": {
			"eiei": {"translate": [],"synonyms": [],"antonyms": [] },
			"translate": {"translate1": [],"translate2": [],"translate3": [] },
			"synonyms": {"synonyms1": [], "synonyms2": [], "synonyms3": [] },
			"antonyms": {"antonyms1": [],"antonyms2": [],"antonyms3": [] },
		}
	}
	