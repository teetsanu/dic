<?php
	// Saving data from form in text file in JSON format
	// From: http://coursesweb.net/php-mysql/

	// check if all form data are submited, else output error message
	if(isset($_POST['vocab']) && isset($_POST['vtranslate1']) && isset($_POST['vtranslate2']) && isset($_POST['vtranslate3']) && isset($_POST['vsynonyms1']) && isset($_POST['vsynonyms2']) && isset($_POST['vsynonyms3']) && isset($_POST['vantonyms1']) && isset($_POST['vantonyms2']) && isset($_POST['vantonyms3']) && isset($_POST['ntranslate1']) && isset($_POST['ntranslate2']) && isset($_POST['ntranslate3']) && isset($_POST['nsynonyms1']) && isset($_POST['nsynonyms2']) && isset($_POST['nsynonyms3']) && isset($_POST['nantonyms1']) && isset($_POST['nantonyms2']) && isset($_POST['nantonyms3'])&& isset($_POST['adjtranslate1']) && isset($_POST['adjtranslate2']) && isset($_POST['adjtranslate3']) && isset($_POST['adjsynonyms1']) && isset($_POST['adjsynonyms2']) && isset($_POST['adjsynonyms3']) && isset($_POST['adjantonyms1']) && isset($_POST['adjantonyms2']) && isset($_POST['adjantonyms3'])) {
		// if form fields are empty, outputs message, else, gets their data
		if(empty($_POST['vocab']) || empty($_POST['vtranslate1']) || empty($_POST['vtranslate2']) || empty($_POST['vtranslate3']) || empty($_POST['vsynonyms1']) || empty($_POST['vsynonyms2']) || empty($_POST['vsynonyms3']) || empty($_POST['vantonyms1']) || empty($_POST['vantonyms2']) || empty($_POST['vantonyms3'])|| empty($_POST['ntranslate1']) || empty($_POST['ntranslate2']) || empty($_POST['ntranslate3']) || empty($_POST['nsynonyms1']) || empty($_POST['nsynonyms2']) || empty($_POST['nsynonyms3']) || empty($_POST['nantonyms1']) || empty($_POST['nantonyms2']) || empty($_POST['nantonyms3']) || empty($_POST['adjtranslate1']) || empty($_POST['adjtranslate2']) || empty($_POST['adjtranslate3']) || empty($_POST['adjsynonyms1']) || empty($_POST['adjsynonyms2']) || empty($_POST['adjsynonyms3']) || empty($_POST['adjantonyms1']) || empty($_POST['adjantonyms2']) || empty($_POST['adjantonyms3'])) {
			echo 'All fields are required';
		}
		else {
			// adds form data into an array
			
			/*
			$vocabdata = array(
				'vocabulary'=> $_POST['vocab'],
				'translate'=> $_POST['translate'],
				'synonyms'=> $_POST['synonyms'],
				'numbers' => array(1, 2, 3, 4, 5, 6),
				'antonyms'=> $_POST['antonyms']
			);
			*/
			
			if($_POST['vocab'] == '-'){
				echo 'Your word contain forbidden character';
			}else{
				$wordToSearchDup = "var ".strtolower($_POST['vocab']);
		
			
				if(strpos(file_get_contents("js/data-json.js"), $wordToSearchDup) !== false) {
					echo "Already Have This Vocaburary";
				}else{
					$orange = "#EEB211";
					$darkblue = "#21526a";
					$purple = "#941e5e";
					$limegreen = "#c1d72e";
					
					$vocabdata = array(
						'nodes'=> array(
								'vocab' => array(
								'label' => strtoupper($_POST['vocab']),
								'color' => $orange,
								'shape' => 'dot',
								'radius' => 60,
								'alpha' => 1
							),
							'verb' => array(
								'label' => 'Verb',
								'color' => $darkblue,
								'shape' => 'dot',
								'radius' => 60,
								'alpha' => 1
							),
							'noun' => array(
								'label' => 'Noun',
								'color' => $purple,
								'shape' => 'dot',
								'radius' => 60,
								'alpha' => 1
							),
							'adj' => array(
								'label' => 'Adj',
								'color' => $limegreen,
								'shape' => 'dot',
								'radius' => 60,
								'alpha' => 1
							),
							'vtranslate' => array(
								'label' => 'Translate',
								'color' => $darkblue,
								'shape' => 'dot',
								'alpha' => 1
							),
							'vsynonyms' => array(
								'label' => 'Synonyms',
								'color' => $darkblue,
								'shape' => 'dot',
								'alpha' => 1
							),
							'vantonyms' => array(
								'label' => 'Antonyms',
								'color' => $darkblue,
								'shape' => 'dot',
								'alpha' => 1
							),
							'vtranslate1' => array(
								'label' => $_POST['vtranslate1'],
								'color' => $darkblue,
								'alpha' => 0
							),
							'vtranslate2' => array(
								'label' => $_POST['vtranslate2'],
								'color' => $darkblue,
								'alpha' => 0
							),
							'vtranslate3' => array(
								'label' => $_POST['vtranslate3'],
								'color' => $darkblue,
								'alpha' => 0
							),
							'vsynonyms1' => array(
								'label' => $_POST['vsynonyms1'],
								'color' => $darkblue,
								'alpha' => 0
							),
							'vsynonyms2' => array(
								'label' => $_POST['vsynonyms2'],
								'color' => $darkblue,
								'alpha' => 0
							),
							'vsynonyms3' => array(
								'label' => $_POST['vsynonyms3'],
								'color' => $darkblue,
								'alpha' => 0
							),
							'vantonyms1' => array(
								'label' => $_POST['vantonyms1'],
								'color' => $darkblue,
								'alpha' => 0
							),
							'vantonyms2' => array(
								'label' => $_POST['vantonyms2'],
								'color' => $darkblue,
								'alpha' => 0
							),
							'vantonyms3' => array(
								'label' => $_POST['vantonyms3'],
								'color' => $darkblue,
								'alpha' => 0
							),
							//
							'ntranslate' => array(
								'label' => 'Translate',
								'color' => $purple,
								'shape' => 'dot',
								'alpha' => 1
							),
							'nsynonyms' => array(
								'label' => 'Synonyms',
								'color' => $purple,
								'shape' => 'dot',
								'alpha' => 1
							),
							'nantonyms' => array(
								'label' => 'Antonyms',
								'color' => $purple,
								'shape' => 'dot',
								'alpha' => 1
							),
							'ntranslate1' => array(
								'label' => $_POST['ntranslate1'],
								'color' => $purple,
								'alpha' => 0
							),
							'ntranslate2' => array(
								'label' => $_POST['ntranslate2'],
								'color' => $purple,
								'alpha' => 0
							),
							'ntranslate3' => array(
								'label' => $_POST['ntranslate3'],
								'color' => $purple,
								'alpha' => 0
							),
							'nsynonyms1' => array(
								'label' => $_POST['nsynonyms1'],
								'color' => $purple,
								'alpha' => 0
							),
							'nsynonyms2' => array(
								'label' => $_POST['nsynonyms2'],
								'color' => $purple,
								'alpha' => 0
							),
							'nsynonyms3' => array(
								'label' => $_POST['nsynonyms3'],
								'color' => $purple,
								'alpha' => 0
							),
							'nantonyms1' => array(
								'label' => $_POST['nantonyms1'],
								'color' => $purple,
								'alpha' => 0
							),
							'nantonyms2' => array(
								'label' => $_POST['nantonyms2'],
								'color' => $purple,
								'alpha' => 0
							),
							'nantonyms3' => array(
								'label' => $_POST['nantonyms3'],
								'color' => $purple,
								'alpha' => 0
							),
							//
							'adjtranslate' => array(
								'label' => 'Translate',
								'color' => $limegreen,
								'shape' => 'dot',
								'alpha' => 1
							),
							'adjsynonyms' => array(
								'label' => 'Synonyms',
								'color' => $limegreen,
								'shape' => 'dot',
								'alpha' => 1
							),
							'adjantonyms' => array(
								'label' => 'Antonyms',
								'color' => $limegreen,
								'shape' => 'dot',
								'alpha' => 1
							),
							'adjtranslate1' => array(
								'label' => $_POST['adjtranslate1'],
								'color' => $limegreen,
								'alpha' => 0
							),
							'adjtranslate2' => array(
								'label' => $_POST['adjtranslate2'],
								'color' => $limegreen,
								'alpha' => 0
							),
							'adjtranslate3' => array(
								'label' => $_POST['adjtranslate3'],
								'color' => $limegreen,
								'alpha' => 0
							),
							'adjsynonyms1' => array(
								'label' => $_POST['adjsynonyms1'],
								'color' => $limegreen,
								'alpha' => 0
							),
							'adjsynonyms2' => array(
								'label' => $_POST['adjsynonyms2'],
								'color' => $limegreen,
								'alpha' => 0
							),
							'adjsynonyms3' => array(
								'label' => $_POST['adjsynonyms3'],
								'color' => $limegreen,
								'alpha' => 0
							),
							'adjantonyms1' => array(
								'label' => $_POST['adjantonyms1'],
								'color' => $limegreen,
								'alpha' => 0
							),
							'adjantonyms2' => array(
								'label' => $_POST['adjantonyms2'],
								'color' => $limegreen,
								'alpha' => 0
							),
							'adjantonyms3' => array(
								'label' => $_POST['adjantonyms3'],
								'color' => $limegreen,
								'alpha' => 0
							),
						),
						'edges'=> array(
							'vocab' => array(
								'verb' => array(),
								'noun' => array(),
								'adj' => array()
							),
							'verb' => array(
								'vtranslate' => array(),
								'vsynonyms' => array(),
								'vantonyms' => array()
							),
								'vtranslate' => array(
									'vtranslate1' => array(),
									'vtranslate2' => array(),
									'vtranslate3' => array()
									),	
								'vsynonyms' => array(
									'vsynonyms1' => array(),
									'vsynonyms2' => array(),
									'vsynonyms3' => array()
									),
								'vantonyms' => array(
									'vantonyms1' => array(),
									'vantonyms2' => array(),
									'vantonyms3' => array()
									),
							'noun' => array(
								'ntranslate' => array(),
								'nsynonyms' => array(),
								'nantonyms' => array()
							),
								'ntranslate' => array(
									'ntranslate1' => array(),
									'ntranslate2' => array(),
									'ntranslate3' => array()
									),
								'nsynonyms' => array(
									'nsynonyms1' => array(),
									'nsynonyms2' => array(),
									'nsynonyms3' => array()
									),
								'nantonyms' => array(
									'nantonyms1' => array(),
									'nantonyms2' => array(),
									'nantonyms3' => array()
									),
							'adj' => array(
								'adjtranslate' => array(),
								'adjsynonyms' => array(),
								'adjantonyms' => array()
							),
								'adjtranslate' => array(
									'adjtranslate1' => array(),
									'adjtranslate2' => array(),
									'adjtranslate3' => array()
									),
								'adjsynonyms' => array(
									'adjsynonyms1' => array(),
									'adjsynonyms2' => array(),
									'adjsynonyms3' => array()
									),
								'adjantonyms' => array(
									'adjantonyms1' => array(),
									'adjantonyms2' => array(),
									'adjantonyms3' => array()
									),
						)
						
					);
					
					// encodes the array into a string in JSON format (JSON_PRETTY_PRINT - uses whitespace in json-string, for human readable)
					$jsondata = "\n\n"."var ".strtolower($_POST['vocab']).' = ';
					$jsondata .= json_encode($vocabdata, JSON_PRETTY_PRINT);
					// saves the json string in "formdata.txt" (in "dirdata" folder)
					// outputs error message if data cannot be saved
					if(file_put_contents('js/data-json.js', $jsondata, FILE_APPEND)){
						echo 'Data successfully saved'."<br>";
						echo "<a href='addWord.php'>Back to Add Vocab</a>"."<br>";
						echo "<a href='index.html'>Back to Index</a>";
						//print_r($vocabdata);
					}else{ 
						echo 'Unable to save data in "js/data-json.js"';
					}
				}
			}
		}
	}else{
		echo 'Form fields not submited';
	}
?>